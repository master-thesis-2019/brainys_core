## Setup the environment

### Dependencies

Dependencies can be either installed with **Anaconda** (strongly recommended) or manually. In the development the Anaconda Distribution was used and following instructions relay on it.

Note that in the development process GPU computing was used. However it is limited to NVIDIA GPUs and requires setting additional libraries such as `CUDA`, `cuDNN` and others. You can choose between `conda_env_gpu.yaml` relying on an NVIDIA GPU or `conda_env_cpu.yaml` which is significantly slower but runs on all platforms.

1. Download and install Anaconda from the [official site](https://www.anaconda.com/distribution/)
1. Open `Anaconda Prompt`.
1. Go to the `../brainys/brainys_core` directory.
1. Create environment and install dependencies by running one of the commands:
    - `conda env create -f conda_env_gpu.yaml` - for GPU computing   
    - `conda env create -f conda_env_cpu.yaml` - for CPU computing

### Re3 model files
Version used during development: <! upload the model and insert here> - **recommended**

Probably most recent version of the model: http://bit.ly/2L5deYF

Unpack and place the files in the ```.../brainys_core/src/libraries/re3/logs/checkpoints``` directory.

### YOLO v3 model files
Version used during development: <! upload the model and insert here> - **recommended**

Probably most recent version of the model: https://pjreddie.com/media/files/yolov3.weights

Convert it as described in the [repo](https://github.com/qqwweee/keras-yolo3)

Then place the converted `*.h5` file in the ```.../brainys_core/src/libraries/yolo_v3/model_data``` directory.