from uuid import uuid4


class TrackedObject:
    def __init__(self, label, boundingBox, id = None):
        if id is None or id == '':
            self.id = uuid4()
        else:
            self.id = id

        self.label = label
        self.bbox = boundingBox
        self.not_detected_count = 0
        self.history_points = []

    def to_str(self):
        return '{} {}'.format(self.label, self.boundingBox.to_str())

    def to_full_str(self):
        return '{} {}, '.format(self.to_str(), 'not_detected_count: ' + self.not_detected_count)

def get_bboxes(tracked_objects):
    """
    Extract bbox property from the list of tracked objects.

    Args:
        tracked_objects (TrackedObject[]): list of tracked objects

    Returns:
        BoundingBox[]: list of bounding boxes
    """

    return [tracked_object.bbox for tracked_object in tracked_objects]


