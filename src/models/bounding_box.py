class BoundingBox:
    def from_scalars(self, left, top, right, bottom):
        self.left = left
        self.top = top
        self.right = right
        self.bottom = bottom

    def to_list(self):
        return [self.left, self.top, self.right, self.bottom]

    def from_list(self, list):
        self.left = list[0]
        self.top = list[1]
        self.right = list[2]
        self.bottom = list[3]

    def get_center_point(self):
        x_center = self.left + self.get_x_length() / 2.0
        y_center = self.top + self.get_y_length() / 2.0

        return x_center, y_center

    def get_center_point_int(self):
        x_center = int(self.left + self.get_x_length() / 2.0)
        y_center = int(self.top + self.get_y_length() / 2.0)

        return x_center, y_center

    def get_x_length(self):
        return self.right - self.left

    def get_y_length(self):
        return self.bottom - self.top

    def get_area(self):
        return self.get_x_length() * self.get_y_length()

    def to_str(self):
        return '[left: {}, top: {}, right: {}, bottom: {}]'\
            .format(int(self.left), int(self.top), int(self.right), int(self.bottom))