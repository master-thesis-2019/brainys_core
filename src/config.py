class Config:
    def __init__(
        self,
        is_debug_mode,
        overlap_margin_ratio,
        offset_margin_ratio,
        detector_skip_frames_count,
        undetected_limit):

        self.is_debug_mode = is_debug_mode
        self.overlap_margin_ratio = overlap_margin_ratio
        self.offset_margin_ratio = offset_margin_ratio
        self.detector_skip_frames_count = detector_skip_frames_count
        self.undetected_limit = undetected_limit
