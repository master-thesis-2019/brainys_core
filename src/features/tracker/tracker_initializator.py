from debug.log_debugger import log_debugger
from debug.visual_debugger import visual_debugger
from features.tracker.geometric_object_comparer import GeometricObjectComparer
from models.bounding_box import BoundingBox
from models.tracked_object import TrackedObject

class TrackerInitializator:
    def __init__(self, config, tracker):
        self.tracker = tracker
        visual_debugger.new_window('trackers_init')
        self.config = config
        self.object_comparer = GeometricObjectComparer(config.overlap_margin_ratio, config.offset_margin_ratio)

    def init_tracker(self, label, detected_bbox, frame):
        log_debugger.print('Start tracking {} {}'.format(label, detected_bbox.to_str()))

        new_object_to_track = TrackedObject(label, detected_bbox)

        tracker_bbox = self.tracker.track_single(
            new_object_to_track.id,
            self._convert_frame_to_tracker_format(frame),
            detected_bbox.to_list())

        new_object_to_track.history_points.append(tracker_bbox.get_center_point_int())

        self.tracker.trackers.append(new_object_to_track)

    def init_tracker_for_multiple_detections(self, detections_to_init_tracker, frame):
        log_debugger.print('Objects to initialize tracking: {}'.format(len(detections_to_init_tracker)))

        for detection in detections_to_init_tracker:
            self.init_tracker(detection.label, detection.bbox, frame)

    def sort_detections_and_trackers(self, detections, trackers):
        """
        Takes detections and trackers then divides them into detections that haven't been tracked yet and trackers that
        haven't been detected.

        Arguments:
            detections {OutItem[]} -- List of detected objects
            trackers {TrackedObject[]} -- List of tracked objects

        Returns:
            OutItem[] -- detections that haven't been tracked yet
            TrackedObject[] -- trackers that haven't been detected
        """

        trackers_copied = trackers.copy()
        detections_copied = detections.copy()

        untracked_detections = []
        matched_trackers = []

        for detection in detections_copied:
            self.compare_detection_with_all_trackers(detection, trackers_copied, untracked_detections, matched_trackers)

        undetected_trackers = trackers_copied

        return untracked_detections, undetected_trackers

    def _convert_frame_to_tracker_format(self, frame):
        return frame[:,:,::-1]

    def compare_detection_with_all_trackers(self, detection, trackers_copied, untracked_detections, matched_trackers):
        for tracker in trackers_copied:
            visual_debugger.clear_image('trackers_init')
            visual_debugger.windows['trackers_init'].paint_detection(detection)
            visual_debugger.windows['trackers_init'].paint_tracker(tracker)
            visual_debugger.windows['trackers_init'].mark_trackers_matched_with_detectors(matched_trackers)
            visual_debugger.display('trackers_init', False)

            is_the_same_object = self.object_comparer.is_the_same_object(
                detection,
                tracker,
                self.config.offset_margin_ratio,
                self.config.overlap_margin_ratio)

            if is_the_same_object:
                trackers_copied.remove(tracker)
                matched_trackers.append(tracker)
                visual_debugger.windows['trackers_init'].mark_tracker_matched_with_detector(tracker)
                visual_debugger.display('trackers_init', False)
                return

        untracked_detections.append(detection)

