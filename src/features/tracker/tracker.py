from libraries.re3.tracker import re3_tracker
from libraries.re3.re3_utils.util import drawing
from libraries.re3.re3_utils.util import bb_util
from libraries.re3.re3_utils.util import im_util

from features.tracker.tracker_initializator import TrackerInitializator
from features.tracker.obsolete_trackers_remover import ObsoleteTrackersRemover
from models.bounding_box import BoundingBox

class Tracker:
    def __init__(self, config):
        self.re3_tracker = re3_tracker.Re3Tracker()
        self.initializator = TrackerInitializator(config, self)
        self.trackers_remover = ObsoleteTrackersRemover(config.undetected_limit)
        self.trackers = []

    def run_initialization(
        self,
        detections_in_current_frame,
        frame):
        """
        Starts the initialization process for detected objects that are not tracked yet. Furthemore, it keeps
        track of tracked objects that are not detected anymore and removes them from the tracking process.

        Arguments:
            detections_in_current_frame {OutItem} -- Objects detected in the current frame by the detector
            frame {OpenCV frame} -- Current frame
        """

        untracked_detections, undetected_trackers = self.initializator.sort_detections_and_trackers(
            detections_in_current_frame,
            self.trackers)

        self.initializator.init_tracker_for_multiple_detections(untracked_detections, frame)

        self.trackers = self.trackers_remover.monitor_undetected_trackers(
            self.trackers,
            undetected_trackers)

    def track_single(self, unique_id, image, starting_box=None):
        tracker_list_bbox = self.re3_tracker.track(unique_id, image, starting_box)

        tracker_bbox = BoundingBox()
        tracker_bbox.from_list(tracker_list_bbox)

        return tracker_bbox

    def update_trackers(self, frame):
        for tracked_object in self.trackers:
            tracker_list_bbox = self.re3_tracker.track(tracked_object.id, frame[:,:,::-1])

            tracker_bbox = BoundingBox()
            tracker_bbox.from_list(tracker_list_bbox)

            tracked_object.bbox = tracker_bbox
            tracked_object.history_points.append(tracker_bbox.get_center_point_int())