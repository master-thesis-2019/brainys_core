from debug.log_debugger import log_debugger
from models.bounding_box import BoundingBox

class GeometricObjectComparer:
    def __init__(self, overlap_margin_ratio, offset_margin_ratio):
        self.overlap_margin_ratio = overlap_margin_ratio
        self.offset_margin_ratio = offset_margin_ratio

    def is_the_same_object(self, detected_object, tracked_object, offset_margin_ratio, overlap_margin_ratio):
        if tracked_object.label == detected_object.label:

            is_overlapping = self._boxes_overlap(
                detected_object.bbox,
                tracked_object.bbox,
                offset_margin_ratio,
                overlap_margin_ratio)

        return is_overlapping

    def _boxes_overlap(self, bbox1, bbox2, offset_margin_ratio, area_margin_ratio):
        smaller_bbox, smaller_area, bigger_bbox, bigger_area = self._find_smaller_and_bigger_bbox(bbox1, bbox2)

        if self._is_area_difference_too_large(smaller_area, bigger_area, area_margin_ratio):
            log_debugger.print('Area exeeds margin, boxes not similar')
            return False

        if self._is_centre_points_offset_too_large(smaller_bbox, bigger_bbox, self.offset_margin_ratio):
            log_debugger.print('Center point offset exceeded')
            return False

        log_debugger.print('Boxes are recognised to represent the same object')
        return True

    def _find_smaller_and_bigger_bbox(self, bbox1, bbox2):
        area_1 = bbox1.get_area()
        area_2 = bbox2.get_area()

        if area_1 <= area_2:
            smaller_bbox = bbox1
            bigger_bbox = bbox2

            smaller_area = area_1
            bigger_area = area_2

        else:
            bigger_bbox = bbox1
            smaller_bbox = bbox2

            bigger_area = area_1
            smaller_area = area_2

        return smaller_bbox, smaller_area, bigger_bbox, bigger_area

    def _is_area_difference_too_large(self, smaller_area, bigger_area, area_margin_ratio):
        return (1 - smaller_area / bigger_area) > area_margin_ratio

    def _is_centre_points_offset_too_large(self, smaller_bbox, bigger_bbox, offset_margin_ratio):
        smaller_center_x, smaller_center_y = smaller_bbox.get_center_point()
        bigger_center_x, bigger_center_y = bigger_bbox.get_center_point()

        x_offset_margin = bigger_bbox.get_x_length() * offset_margin_ratio
        y_offset_margin = bigger_bbox.get_y_length() * offset_margin_ratio

        x_offset_exceeded = abs(smaller_center_x - bigger_center_x) > x_offset_margin
        y_offset_exceeded = abs(smaller_center_y - bigger_center_y) > y_offset_margin

        return x_offset_exceeded or y_offset_exceeded
