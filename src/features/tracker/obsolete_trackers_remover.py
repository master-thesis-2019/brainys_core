class ObsoleteTrackersRemover:
    def __init__(self, undetected_limit):
        self.undetected_limit = undetected_limit

    def monitor_undetected_trackers(self, all_trackers, undetected_trackers):
        """
        Incerements 'not_detected_counter' for objects that were not detected in the current frame and
        clears the counter for other trackers. Objects whose counter exceeds the 'undetected_limit'
        value are excluded from the returned list.

        Arguments:
            all_trackers {TrackedObject[]} -- All the trackers
            undetected_trackers {TrackedObject[]} -- Tracked objects that haven't been detected

        Returns:
            List of tracked objects that did not exceed undetected_limit.
        """

        from debug.visual_debugger import visual_debugger

        all_trackers_copy = all_trackers.copy()

        for obj in undetected_trackers:
            obj.not_detected_count += 1
            if obj.not_detected_count >= self.undetected_limit:
                visual_debugger.windows['main'].paint_obsolete_tracker(obj)
                all_trackers_copy.remove(obj)

        return all_trackers_copy
