
from PIL import Image

from libraries.yolo_v3.yolo import YOLO
from libraries.yolo_v3.yolo3.out_item import collection_to_str
from debug.log_debugger import log_debugger


class Detector:
    def __init__(self, skip_frames_count):
        self.skip_frames_count = skip_frames_count
        self.frames_skipped = skip_frames_count
        self.yolo = YOLO()

    def run(self, frame):
        log_debugger.print('Detector is starting')

        yolo_format_img = Image.fromarray(frame)
        predicted_objects = self.yolo.predict(yolo_format_img)

        log_debugger.print('Detector has finished')

        detected_people = self._get_detected_people(predicted_objects)
        log_debugger.print(collection_to_str(detected_people))

        return detected_people

    def close_session(self):
        self.yolo.close_session()

    def is_wake_up_moment(self):
        self.frames_skipped += 1
        is_wake_up_moment = self.frames_skipped >= self.skip_frames_count

        if is_wake_up_moment:
            self.frames_skipped = 0

        return is_wake_up_moment

    def _get_detected_people(self, predicted_objects):
        return [obj for obj in predicted_objects if obj.label == 'person']