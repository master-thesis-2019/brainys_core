import cv2

class Painter:
    def rectangle(self, image, bbox, color, thickness=4):
        """
        Draws a bounding box on the image.

        Args:
            image (opencv image): The image to draw the shape on.
            bbox (BoundingBox): bounding box to be drawn.
            color ([b, g, r]): color of the bbox.
        """

        return cv2.rectangle(
                    image,
                    (int(bbox.left), int(bbox.top)),
                    (int(bbox.right), int(bbox.bottom)),
                    color,
                    thickness=thickness)

    def rectangles(self, image, bboxes, color):
        """
        Draws list of bounding boxes at the current self.image.

        Args:
            image (opencv image): The image to draw the shape on.
            bboxes (BoundingBox[]): list of bounding boxes to be drawn.
            color ([b, g, r]): the color of bboxes.
        """

        for bbox in bboxes:
            image = self.rectangle(image, bbox, color)

        return image

    def cross(self, image, bbox, color, thickness=5):
        """
        Draws a cross on the image.

        Args:
            image (opencv image): The image to draw the shape on.
            bbox (BoundingBox): Rectangle that defines the shape of the cross to be painted.
            color ([b, g, r]): color of the cross
            thickness (int, optional): Defaults to 5. The thickness of lines in pixels.

        Returns:
            opencv image: Image with painted cross
        """

        start_point_1 = (bbox.left, bbox.top)
        end_point_1 = (bbox.right, bbox.bottom)
        image = cv2.line(image, start_point_1, end_point_1, color, thickness)

        start_point_2 = (bbox.left, bbox.bottom)
        end_point_2 = (bbox.right, bbox.top)
        image = cv2.line(image, start_point_2, end_point_2, color, thickness)

        return image

    def multiline(self, image, points, color, thickness=5):
        """
        Paints a poly line as iterative drawing of multiple straight lines where each the straight line
        is drawn between previous point and the next one.

        Args:
            image (opencv image): The image to paint the shape on.
            points (array of tuples of points): Sequentially ordered points.
            color ([b, g, r]): Color of the shape.
            thickness (int, optional): Defaults to 5. The thickness of lines in pixels.
        """

        for i in range(1, len(points)):
            current_start_point = points[i - 1]
            current_stop_point = points[i]

            image = cv2.line(image, current_start_point, current_stop_point, color, thickness=thickness)

        return image
