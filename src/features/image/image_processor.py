import cv2

from libraries.re3.constants import OUTPUT_WIDTH
from libraries.re3.constants import OUTPUT_HEIGHT
from libraries.re3.constants import PADDING

from features.image.painter import Painter

tracker_bbox_color = [0, 0, 255]

class ImageProcessor:
    def __init__(self, window_name, path_to_video):
        self.painter = Painter()
        self.init_provider(path_to_video)

    def init_provider(self, path_to_video):
        self.img_provider = cv2.VideoCapture(path_to_video)

    def loop_through_video(self, callback_run_in_every_frame):
        while True:
            frame = self.get_next_frame()
            if frame is None:
                break # End of video.

            callback_run_in_every_frame(frame)

        cv2.destroyAllWindows()

    def get_next_frame(self):
        ret_val, frame = self.img_provider.read()
        return frame