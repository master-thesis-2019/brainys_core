from models.bounding_box import BoundingBox

class OutItem:
    def __init__(self, label, score, leftBBox, topBBox, rightBBox, bottomBBox):
        self.label = label
        self.score = score

        self.bbox = BoundingBox()
        self.bbox.from_scalars(leftBBox, topBBox, rightBBox, bottomBBox)

    def to_str(self):
        return '{} {:.2f} [(l:{} t:{}) (r:{} b:{})]'.format(
            self.label,
            self.score,
            self.bbox.left,
            self.bbox.top,
            self.bbox.right,
            self.bbox.bottom)

def collection_to_str(collection):
    out = 'Items count: '.format(len(collection))
    for item in collection:
        out += '\n{}'.format(item.to_str())

    return out

def get_bboxes(detections):
    """
    Extract bbox property from the list of detected objects.

    Args:
        detections (OutItem[]): list of the detected objects

    Returns:
        BoundingBox[]: list of bounding boxes
    """

    return [detection.bbox for detection in detections]
