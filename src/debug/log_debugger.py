from debug.singleton import Singleton


class LogDebugger(metaclass=Singleton):
    def set_is_debug_mode(self, is_debug_mode):
        self.is_debug_mode = is_debug_mode

    def print(self, info):
        if self.is_debug_mode:
            print(info)

# The singleton instance to interact with from external modules
log_debugger = LogDebugger()