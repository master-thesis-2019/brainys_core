import cv2

from features.image.painter import Painter
from libraries.yolo_v3.yolo3.out_item import get_bboxes as get_detections_bboxes
from models.tracked_object import get_bboxes as get_trackers_bboxes

PADDING = 2

class VisualDebuggerWindow:
    def __init__(self, image):
        self.image = image
        self.painter = Painter()

        self.tracker_color = [0, 0, 255]
        self.detection_color = [255, 0, 255]
        self.trajectory_color = [0, 100, 255]

    def paint_detection(self, detection):
        self.image = self.painter.rectangle(self.image, detection.bbox, self.detection_color)

    def paint_detections(self, detections):
        bboxes = get_detections_bboxes(detections)
        self.image = self.painter.rectangles(self.image, bboxes, self.detection_color)

    def paint_tracker(self, tracker):
        self.image = self.painter.rectangle(self.image, tracker.bbox, self.tracker_color)

    def paint_trackers(self, trackers):
        bboxes = get_trackers_bboxes(trackers)
        self.image = self.painter.rectangles(self.image, bboxes, self.tracker_color)

    def mark_tracker_matched_with_detector(self, tracker):
        self.image = self.painter.rectangle(self.image, tracker.bbox, [0, 255, 0], -1)

    def mark_trackers_matched_with_detectors(self, trackers):
        for tracker in trackers:
            self.mark_tracker_matched_with_detector(tracker)

    def paint_obsolete_tracker(self, tracker):
        """
        Draws a marker (cross) on the image where a tracker was removed in the current frame.

        Args:
            tracker (TrackedObject): the object that was tracked
        """

        color = [244, 241, 66]
        self.image = self.painter.cross(self.image, tracker.bbox, color)

    def paint_trajectory(self, tracker):
        """
        Paints the history points of a tracked object as a poly line

        Args:
            tracker (TrackedObject): The tracked object.
        """

        self.image = self.painter.multiline(self.image, tracker.history_points, self.trajectory_color)

    def paint_trajectories(self, trackers):
        """
        Paints history points for each tracked objects as poly line

        Args:
            trackers (TrackedObject[]): Tracked objects.
        """

        for tracker in trackers:
            self.paint_trajectory(tracker)

