import cv2
import numpy as np

from debug.singleton import Singleton
from debug.log_debugger import log_debugger
from debug.visual_debugger_window import VisualDebuggerWindow

# Drawing constants
OUTPUT_WIDTH = 1024
OUTPUT_HEIGHT = 576


class VisualDebugger(metaclass=Singleton):
    def __init__(self):
        self.windows = { }
        self.clean_image = None

    def set_img_for_all_windows(self, image):
        """
        Sets the same image for all windows. Might be used to set a clean current frame at the beginning of the loop.

        Args:
            image (opencv image): the image to be set
        """

        self.clean_image = image.copy()

        for window in self.windows.values():
            window.image = image

    def clear_image(self, window_id):
        """
        Wipes all drawings on the image within the window.

        Args:
            window_id (str): ID of the window.
        """

        self.windows[window_id].image = self.clean_image.copy()

    def new_window(self, window_id):
        """
        Creates a new instance of VisualDebuggerWindow which makes a new window with its own set of features
        to be displayed.

        Args:
            window_id (str): ID of the window. It will be used to access the window in other methods.
        """

        self.windows[window_id] = VisualDebuggerWindow(self.clean_image)

    def display(self, window_id, hold_until_key_stroked=True, width=OUTPUT_WIDTH, height=OUTPUT_HEIGHT):
        """
        Displays the image in the window with given window_id.

        Args:
            window_id (str): unique ID of the window to access.
            hold_until_key_stroked (bool, optional): Defaults to True. Holds the whole program until a key is
            stroked when the window is focused if set to True.
            width (int, optional): Defaults to OUTPUT_WIDTH. Width of the window.
            height (int, optional): Defaults to OUTPUT_HEIGHT. Height of the window.
        """

        window_name = "BrainyS - {}".format(window_id)
        cv2.namedWindow(window_name, cv2.WINDOW_NORMAL)
        cv2.resizeWindow(window_name, OUTPUT_WIDTH, OUTPUT_HEIGHT)
        cv2.imshow(window_name, self.windows[window_id].image)

        if hold_until_key_stroked:
            log_debugger.print('The program is on hold. Press any key within the {} debug window to continue.'.format(window_id))
            cv2.waitKey(0)
            log_debugger.print('The program is resumed.')
        else:
            cv2.waitKey(1)

# The singleton instance to interact with from external modules
visual_debugger = VisualDebugger()