import sys
sys.path.append('./../libraries/')

import cv2
import argparse

from config import Config
from models.bounding_box import BoundingBox
from models.tracked_object import TrackedObject
from debug.log_debugger import log_debugger
from debug.visual_debugger import visual_debugger
from features.detector import Detector
from features.tracker.tracker import Tracker
from features.image.image_processor import ImageProcessor


class BrainysCore:
    def __init__(self, config):
        log_debugger.set_is_debug_mode(config.is_debug_mode)
        visual_debugger.new_window('main')
        visual_debugger.new_window('trajectories')
        self.img_processor = ImageProcessor('BrainyS', './../../videos/TownCentre.avi')
        self.tracker = Tracker(config)
        self.detector = Detector(config.detector_skip_frames_count)

    def start(self):
        """
        Starts the BrainyS Core module. The algorithm is executed until the end of a video.
        """

        self.img_processor.loop_through_video(self.run)

    def run(self, frame):
        """
        Runs the whole algorithm once for a frame from a video that has been passed in the config.
        """

        visual_debugger.set_img_for_all_windows(frame)

        self.tracker.update_trackers(frame)

        if self.detector.is_wake_up_moment():
            detected_people = self.detector.run(frame)
            self.tracker.run_initialization(detected_people, frame)

            visual_debugger.windows['main'].paint_detections(detected_people)

        visual_debugger.windows['main'].paint_trackers(self.tracker.trackers)
        visual_debugger.windows['main'].paint_trajectories(self.tracker.trackers)
        visual_debugger.display('main', False)

def setup_argparse():
    """
    Gets config values form the standard input
    """

    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('-d', '--is_debug_mode', action='store_true', help='Display debugging information')

    return arg_parser.parse_args()

if __name__ == "__main__":
    args = setup_argparse()

    config = Config(
        args.is_debug_mode,
        overlap_margin_ratio=0.4,
        offset_margin_ratio=0.2,
        detector_skip_frames_count=3,
        undetected_limit=2)

    brainys = BrainysCore(config)
    brainys.start()
